//
//  AppDelegate.h
//  p01-deepakkaku
//
//  Created by Deepak Kaku on 1/30/16.
//  Copyright © 2016 Deepak Kaku. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

