//
//  main.m
//  p01-deepakkaku
//
//  Created by Deepak Kaku on 1/30/16.
//  Copyright © 2016 Deepak Kaku. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
