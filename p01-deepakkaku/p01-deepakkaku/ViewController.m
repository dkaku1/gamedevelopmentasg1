//
//  ViewController.m
//  p01-deepakkaku
//
//  Created by Deepak Kaku on 1/30/16.
//  Copyright © 2016 Deepak Kaku. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize helloworld;


-(IBAction)clicked:(id)sender
{
    [helloworld setText:@"Deepak completed his first assignment"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end